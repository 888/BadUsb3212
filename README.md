# BadUsb3212

Bad-USB variant with the Atmega32u4 and the ESP8266 chips together on an usb PCB with a microSD card.

The PCB is marked with "bad usb" and "dm-3212" and can be bought on sites like aliexpress.

Should be able to program it with Ducky-script or simular.

I think the software is mainly the work of https://github.com/spacehuhn/wifi_ducky.

See the **[wiki](https://codeberg.org/888/BadUsb3212/wiki)** page for instructions how to flash the chips and run some code.


[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)